from dataclasses import dataclass
from os import system


@dataclass(frozen=True)
class Const:
    edge:str = "edge"
    chrome:str = "chrome"
    launch_cmd = {(edge, False): "start msedge",
                  (edge, True): "start msedge --inprivate",
                  (chrome, False): "start chrome",
                  (chrome, True): "start chrome --incognito"}


def open_url(url:str, browser:str=Const.edge, incognito:bool=False):
    """opens a single given url"""
    assert isinstance(url, str)
    assert isinstance(browser, str)
    _b = browser.lower()
    assert _b in (Const.edge, Const.chrome)
    assert isinstance(incognito, bool)
    key = (_b, incognito)
    cmd = Const.launch_cmd[key]
    site_cmd = f"{cmd} {url}"
    _ = system(site_cmd)


def main():
    site = r"https://docs.python.org/3/library/webbrowser.html"
    for _ in range(3):
        open_url(site)
    return

if __name__ == "__main__":
    main()
