# browsing

basic url browsing for microsoft edge

uses cmd prompts in order to bypass the webbrowser library inability to browse in incognito mode


``` python
from browsing import open_url

site = r"https://docs.python.org/3/library/index.html"
open_url(site)
```
